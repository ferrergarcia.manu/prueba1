package com.eciformacion.ejercicio4.gestor;
//testing git
import java.util.*;
import java.util.Map.Entry;

public class GestorStockCamiseta {
	private HashMap<String,Integer> stock;

	public GestorStockCamiseta() {
		this.stock = new HashMap<String, Integer>();
	}
	
	public void aņadirCamiseta(String ref, int cantidad) {
		if (!this.stock.containsKey(ref)) {
			this.stock.put(ref, cantidad);
		} else {
			this.stock.put(ref, this.stock.get(ref)+cantidad);
		}
	}
	
	public void retirarCamiseta(String ref, int cantidad) {
		if (!this.stock.containsKey(ref)) {
			System.out.println("No hay camisetas");
		} else {
			this.stock.put(ref, this.stock.get(ref)-cantidad);
			if(this.stock.get(ref) <= 0) this.stock.remove(ref);
		}
	}
	
	public Map<String, Integer> buscarCamiseta(String iniref) {
		HashMap<String, Integer> camisetas = new HashMap<>();
		//System.out.println("hola");
		this.stock.entrySet().stream().filter(p -> p.getKey().startsWith(iniref)).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});
		/*for (Iterator iterator = this.stock.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Integer> e = (Entry) iterator.next();
			if(e.getKey().startsWith(iniref)) camisetas.put(e.getKey(), e.getValue());
			
		}*/
		return camisetas;
	}
	
	public HashMap<String, Integer> buscarCamisetaMaxStock(int limite){
		HashMap<String, Integer> camisetas = new HashMap<>();
		this.stock.entrySet().stream().filter(p -> p.getValue() > limite).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});
		/*for (Iterator iterator = this.stock.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, Integer> e = (Entry) iterator.next();
			if(e.getValue() > limite) camisetas.put(e.getKey(), e.getValue());
					
		}		*/
		return camisetas;
	}
	
	public HashMap<String, Integer> buscarCamisetaMinStock(int limite){
		HashMap<String, Integer> camisetas = new HashMap<>();
		this.stock.entrySet().stream().filter(p -> p.getValue() < limite).forEach(p -> {
			camisetas.put(p.getKey(), p.getValue());
		});	
		return camisetas;
	}
	
	public HashMap<String,Integer> getStock() {
		return stock;
	}

	public void setStock(HashMap<String,Integer> stock) {
		this.stock = stock;
	}
	
	public static void main(String[] args) {
		GestorStockCamiseta stock = new GestorStockCamiseta();
		stock.aņadirCamiseta("hola", 5);
		stock.aņadirCamiseta("hola", 3);
		stock.aņadirCamiseta("holamundo", 7);
		stock.aņadirCamiseta("holaquetal", 2);
		stock.aņadirCamiseta("fernandoquetal", 5);
		stock.aņadirCamiseta("fernandtal", 5);
		stock.aņadirCamiseta("fandoquetal", 8);
		System.out.println(stock.buscarCamiseta("hola"));
		System.out.println(stock.buscarCamisetaMaxStock(4));
		System.out.println(stock.buscarCamisetaMinStock(8));
		stock.retirarCamiseta("hola", 3);
		System.out.println("/////");
		System.out.println(stock.buscarCamiseta("hola"));

	}
}
